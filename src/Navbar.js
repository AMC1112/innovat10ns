import { Link, useMatch, useResolvedPath } from "react-router-dom"
import TextLogoNoBg from "./images/TextLogo-no-bg.png"

export default function Navbar() {
  return (
    <>
      <nav className="nav">
        <Link to="/" className="site-title">
          innovat10ns
        </Link>
        <ul className="Nav-Bar-Categories">
          <CustomLink to="/about">ab0ut us</CustomLink>
          <CustomLink to="/how">h0w we 1nn0vate</CustomLink>
          <CustomLink to="/tenants">1nn0vat10ns tenants</CustomLink>
          <CustomLink to="/industries">1ndustr1es we serve</CustomLink>
          <CustomLink to="/products">0ur pr0ducts</CustomLink>
          <CustomLink to="/services">0ur serv1ces</CustomLink>
          <CustomLink to="/tv">tv sh0w</CustomLink>
          <CustomLink to="/careers">can y0u 1nn0vate?</CustomLink>
          <CustomLink to="/contact">c0ntact us</CustomLink>
        </ul>
      </nav>
      <div className="nav-accent" />
    </>
  )
}

function CustomLink({ to, children, ...props }) {
  const resolvedPath = useResolvedPath(to)
  const isActive = useMatch({ path: resolvedPath.pathname, end: true })

  return (
    <li className={isActive ? "active" : ""}>
      <Link to={to} {...props}>
        {children}
      </Link>
    </li>
  )
}
